QT +=               \
    gui             \
    widgets         \
    core            \

TEMPLATE = lib
DEFINES += ORGBEXAMPLEPLUGIN_LIBRARY

CONFIG += c++11

SOURCES +=                                          \
    ORGBExamplePlugin.cpp                           \

HEADERS += \
    ORGBExamplePlugin.h                             \
    Dependencies/ResourceManager.h                  \
    Dependencies/RGBController/RGBController.h      \
    Dependencies/IResourceManager.h                 \
    Dependencies/i2c_smbus/i2c_smbus.h              \
    Dependencies/NetworkClient.h                    \
    Dependencies/NetworkServer.h                    \
    Dependencies/SettingsManager.h                  \
    Dependencies/NetworkProtocol.h                  \
    Dependencies/net_port.h                         \
    Dependencies/json.hpp                           \
    ORGBPluginInterface.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
